import numpy as np

X = np.array(([3,5], [5,1], [10,2]), dtype=float)
Y = np.array(([75], [82], [93]), dtype=float)

X = X/np.amax(X, axis=0)
Y = Y/100

class Neural_Network(object):
    def __init__(self):
        self.inputLayerSize = 2
        self.outputLayerSize = 1
        self.hiddenLayerSize = 3

        self.W1 = np.random.rand(self.inputLayerSize, \
                                 self.hiddenLayerSize)
        self.W2 = np.random.rand(self.hiddenLayerSize, \
                                 self.outputLayerSize)

    def forward(self, X):
        self.z2 = np.dot(X, self.W1)
        self.a2 = Neural_Network.sigmoid(self.z2)
        self.z3 = np.dot(self.a2, self.W2)
        yHat = Neural_Network.sigmoid(self.z3)
        return yHat

    def sigmoid(z):
        return 1/(1+np.exp(-z))

    def sigmoidPrime(z):
        return np.exp(-z)/((1+np.exp(-z))**2)

    def costFunctionPrime(self, X, Y):
        self.yHat = self.forward(X)
        delta3 = np.multiply(-(Y-self.yHat, Neural_Network.sigmoidPrime(self.z3)))
        dJdW2 = np.dot(self.a2.T, delta3)

        delta2 = np.dot(delta3, self.W2.T)*Neural_Network.sigmoidPrime(self.z2)
        dJdW1 = np.dot(X.T, delta2)

        return dJdW1, dJdW2

    def getParams(self):
        params = np.concatenate((self.W1.ravel(), self.W2.ravel()))
        return params

    def setParams(self, params):
        W1_start = 0
        W1_end = self.hiddenLayerSize*self.inputLayerSize
        self.W1 = np.reshape(params[W1_start:W1_end], \
                             (self.inputLayerSize, self.hiddenLayerSize))
        W2_end = W1_end + self.hiddenLayerSize*self.outputLayerSize
        self.W2 = np.reshape(params[W1_end:W2_end], \
                             (self.hiddenLayerSize, self.outputLayerSize)
    def computeGradient(self, X, Y):
        dJdW1, dJdW2 = self.costFunctionPrime(X, Y)
        return np.concatenate((dJdW1.ravel(), dJdW2.ravel()))

    def computeNumericalGradient(N, X, Y):
        paramsInitial = N.getParams()
        numgrad = np.zeros(paramsInitial.shape)
        perturb = np.zero(paramsInitial.shape)
        e = 1e-4

        for p in range(len(paramsInitial)):
            perturb[p] = e
            N.setParams(paramsInitial + perturb)
            loss2 = N.costFunction(X,Y)

            N.setParams(paramsInitial - perturb)
            loss1 = N.costFunction(X,Y)

            numgrad[p] = (loss2-loss1) / (2*e)

            perturb[p] = 0

        N.setParams(paramsInitial)

        return numgrad

NN = Neural_Network()

numgrad = NN.computeNumericalGradient(NN, X, Y)
grad = NN.computeGradient(X, Y)
print(numgrad)
print(grad)
#yHat = NN.forward(X)
#cost1 = NN.costFunctionPrime(X,Y)

#dJdW1, dJdW2 = NN.costFunctionPrime(X,Y)

#scalar = 3
#NN.W1 = NN.W1 + scalar*dJdW1
#NN.W2 = NN.W2 + scalar*dJdW2
#cost2 = NN.costFunctionPrime(X,Y)
#print(cost1, cost2)

#dJdW1, dJdW2 = NN.costFunctionPrime(X,Y)
#NN.W1 = NN.W1 + scalar*dJdW1
#NN.W2 = NN.W2 + scalar*dJdW2
#cost3 = NN.costFunctionPrime(X,Y)
#print(cost2, cost3)

